#!/usr/bin/env python

import os
import multiprocessing
import subprocess
import glob
import shutil
import cPickle as pickle
from time import ctime

from ase.calculators.calculator import Calculator
from ase.io.trajectory import TrajectoryWriter
from ase import io

from amp.model import LossFunction
from amp import Amp
from amp.utilities import TrainingConvergenceError, Logger


class QMML(Calculator):
    """ Quantum Mechanics - Machine Learning (QMML) calculator
        based on AMP and a DFT calculator. """
    implemented_properties = ['energy', 'forces']

    def __init__(self, selection, qmcalc, mmcalc, qmpbc=None, qmvac=None,
                 qmcell=None, retrain=None, traintraj=[], restart=False,
                 ncores=None, intersteps=1):
        '''
        parameters:

        selection: list of int, QM region atoms indices
            Selection out of all the atoms that belong to the QM part.
        qmcalc: Calculator object or string 'gpaw'
            QM-calculator. If it's gpaw, a 'gpaw_driver.py' file needed.
        mmcalc: Calculator object
            MM-calculator. We use AMP here.
        qmpbc: one or three bool, default is None
            None, the same as the atoms object
            Periodic boundary conditions flags of the QM region.
            Examples: True, False, 0, 1, (1, 1, 0), (True, False, False).
        qmvac: float, default is None
            None, the same unit cell size as the whole Atoms object
            If vacuum=10.0 there will thus be 10 Angstrom of vacuum
            on each side of axis=0 (x axis).
        qmcell: 3x3 matrix or length 3 or 6 vector
            default is None, same as the whole Atoms object unit cell
            QM region unit cell
        retrain: dict or None, default is None
            None, the AMP will not be retrained.
            Dict, it passes the convergence criteria to retrain AMP by QM
            calculations.
            such as {'energy_maxresid': 2.0e-3,
                     'force_maxresid': 5.0e-2}
                     {'energy_rmse': 0.001,
                      'energy_maxresid': None,
                      'force_rmse': 0.005,
                      'force_maxresid': None, }
        traintraj: Usually it's a list of atoms object, images used to train
            AMP.  Can be any image format that AMP training can accept.
        restart: Boolean, apply only when retrain=True
        intersteps: int > 0, the number of calculator calls between AMP
            retrains. Applies only when retrain=True.
            Default is 1, the retrain happens at every call.
        ncores: int, the number of total cpus.
            If None, guess the ncores by multiprocessing module for GPAW calc.
        '''
        self.log = Logger('qmml.log')
        self.log('~' * 50)
        self.log('Starting up: {}'.format(ctime()))
        self.selection = selection
        self.qmcalc = qmcalc
        self.mmcalc = mmcalc
        self.qmatoms = None
        self.qmpbc = qmpbc
        self.qmvac = qmvac
        self.qmcell = qmcell
        self.qmtraj = './qmtraj.traj'
        self.retrain = retrain
        if self.retrain:
            self.traintraj = traintraj
            self.restart = restart
            self.callcnt = 0
            self.intersteps = intersteps

        if ncores:
            self.ncores = ncores
        elif self.qmcalc == 'gpaw':
            self.ncores = multiprocessing.cpu_count()

        try:
            self.name = ('QM region {0} - QM region {1} + All {1}'
                         .format(qmcalc.name, mmcalc.name))
        except:  # Dacapo & GPAW
            self.name = ('QM region - QM region {0} + All {0}'
                         .format(mmcalc.name))

        Calculator.__init__(self)

    def initialize_qm(self, atoms):
        constraints = atoms.constraints
        atoms.constraints = []
        self.qmatoms = atoms[self.selection]
        atoms.constraints = constraints
        if self.qmpbc:
            self.qmatoms.pbc = self.qmpbc
        if self.qmcell is not None:
            self.qmatoms.set_cell(self.qmcell, scale_atoms=False)
            self.qmatoms.center()
        if self.qmvac:
            self.qmatoms.center(vacuum=self.qmvac, axis=0)

    def calculate(self, atoms, properties, system_changes):
        log = self.log
        log('='*45)
        log('Calculation started. {}'.format(ctime()), tic='calc')
        Calculator.calculate(self, atoms, properties, system_changes)
        self.initialize_qm(atoms)

        if self.retrain and self.restart:
            self.retrain_amp()
            self.restart = False

        log('Calling QM calculator.', tic='qm')
        if self.qmcalc == 'gpaw':
            qmenergy, qmforces = self.gpaw_calculate()
        else:
            for item in glob.iglob('*.nc'):  # for Dacapo
                self.qmcalc.restart()
            self.qmatoms.set_calculator(self.qmcalc)
            qmenergy = self.qmcalc.get_potential_energy(self.qmatoms)
            qmforces = self.qmcalc.get_forces(self.qmatoms)
            writer = TrajectoryWriter(self.qmtraj, mode='a',
                                      atoms=self.qmatoms)
            writer.write()
        log('QM calculator finished.', toc='qm')

        if self.retrain:
            log('Calling retrain Amp.', tic='calling_retrain')
            if self.callcnt == 0:
                self.callcnt = self.intersteps
                self.retrain_amp()
            log('Done calling retrainn Amp.', toc='calling_retrain')
        else:
            log('Not calling retrain because self.retrain is False.')

        energy = self.mmcalc.get_potential_energy(atoms)
        log('MMcalc total system energy: {:.5f} eV'.format(energy))
        forces = self.mmcalc.get_forces(atoms)
        log('QMcalc core atoms energy:   {:.5f} eV'.format(qmenergy))
        energy += qmenergy
        mmenergy = self.mmcalc.get_potential_energy(self.qmatoms)
        log('MMcalc core atoms energy:   {:.5f} eV'.format(mmenergy))
        energy -= mmenergy
        log('Final energy:               {:.5f} eV'.format(energy))
        forces[self.selection] += qmforces
        forces[self.selection] -= self.mmcalc.get_forces(self.qmatoms)
        self.results['energy'] = energy
        self.results['forces'] = forces
        if self.retrain:
            self.callcnt -= 1
        log('Calculation finished.', toc='calc')

    def retrain_amp(self):
        log = self.log
        log('retrain_amp called.', tic='train')
        if self.restart:
            images = self.traintraj
            files = [i for i in os.listdir('./amp-checkpoints/')]
            files.sort(key=lambda x: os.path.getmtime(
                os.path.join('./amp-checkpoints/', x)))
            shutil.copyfile(os.path.join('./amp-checkpoints/', files[-1]),
                            './restart.amp')
            calc = Amp.load('./restart.amp')
            log('  Loaded from checkpoint')
        else:
            log('  Reading images.')
            images = self.readimages()
            calc = self.mmcalc
            log('  Using stored self.mmcalc and self.readimages.')
        # Use the max force residual as the criterion.
        calc.model.lossfunction = LossFunction(convergence=self.retrain)
        log('  Trying to train.')
        try:
            log('  Called calc.train.')
            calc.train(images=images, overwrite=True)
            log('  Done calling calc.train.')
        except TrainingConvergenceError:
            calc = Amp.load('./amp-untrained-parameters.amp')
            os.rename('./amp-untrained-parameters.amp',
                      '1-untrained-parameters.amp')
            log('  Not successfully trained. Proceeding.')
        else:
            log('  Successfully trained.')
        log('retrain_amp done.', toc='train')
        self.mmcalc = calc

    def readimages(self):
        atoms = io.read(self.qmtraj, index=-1)
        self.traintraj.append(atoms)
        return self.traintraj

    def gpaw_calculate(self, tmpatoms='gpaw_driver.pckl'):
        pickle.dump((self.qmatoms), open(tmpatoms, 'wb'))
        gpaw_driver = './gpaw_driver.py'
        # self.qmtraj is written in the gpaw_driver.py
        command = 'mpiexec -n %d python %s %s %s > gpaw.stdout 2> gpaw.stderr'
        result = subprocess.call(command % (self.ncores,
                                            gpaw_driver,
                                            tmpatoms,
                                            self.qmtraj),
                                 shell=True)
        if result != 0:
            raise RuntimeError('Error in GPAW call. See gpaw.stderr for '
                               'details.')
        gpawatoms = io.read(self.qmtraj, index=-1)
        qmenergy = gpawatoms.get_potential_energy()
        qmforces = gpawatoms.get_forces(apply_constraint=False)
        os.remove(tmpatoms)
        return qmenergy, qmforces
