# QMML #

QMML is using a machine learning (ML) algorithm for atomistic calculations based on quantum mechanics (QM) calculations and AMP.

## Requirements ##
* Atomic Simulation Environment (ASE) https://wiki.fysik.dtu.dk/ase/
* AMP http://amp.readthedocs.io/en/latest/index.html