#!/usr/bin/env python
import sys, os
sys.path.append('../..')

from QMML import QMML
from amp import Amp
from amp.descriptor.gaussian import Gaussian
from amp.model.neuralnetwork import NeuralNetwork
from ase import Atoms, io
from ase.visualize import view
from ase.constraints import FixAtoms
from ase.optimize import BFGS
from ase.calculators.emt import EMT

def readimages(path):
    timages = []
    for adir in os.listdir(path):
        if os.path.isdir(os.path.join(path, adir)):
            for ftraj in os.listdir(os.path.join(path, adir)):
                if '.traj' in ftraj:
                    images = io.read(os.path.join(path, adir, ftraj),
                                    index=':')
                    timages.extend(images)
    return timages

atoms = io.read('./Cu.traj')
atoms.set_pbc((0,1,0))
indices=[atom.index for atom in atoms if atom.tag==0]
atoms.set_constraint(FixAtoms(indices=indices))
selection = range(27,54)

ampcalc = Amp.load('./calc.amp')

retrain={'energy_maxresid': 2.5e-3,
         'force_maxresid': 4.5e-2}
oldimages = readimages('/gpfs/data/ap31/coppertraj/pwtraj-potentialenergy/')

calc = QMML(selection=selection,
            qmcalc='gpaw',
            mmcalc=ampcalc,
            qmvac=5,
            retrain=retrain,
            traintraj=oldimages,
            ncores=16)

atoms.set_calculator(calc)

dyn = BFGS(atoms, logfile='qn.log', trajectory='qn.traj' )
dyn.run(fmax=0.05)
print atoms.get_potential_energy()
