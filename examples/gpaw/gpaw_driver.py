#!/usr/bin/env python

import os
os.system('export OPENBLAS_NUM_THREADS=1')
import sys
import cPickle as pickle

from ase import Atoms, io
from ase.io.trajectory import TrajectoryWriter
from gpaw import GPAW, FermiDirac, Mixer, PW
import gpaw.mpi as mpi

calc = GPAW(txt='gpaw_driver.txt',
            h=0.18,
            xc='RPBE',
            kpts=(1,4,1),
            occupations=FermiDirac(0.1),
            eigensolver='rmm-diis',
            mode = PW(340.15, dipole_corr_dir=2),
            mixer=Mixer(beta=0.1, nmaxold=5, weight=50),
            spinpol=False,
            maxiter=400)

atoms = pickle.load( file(sys.argv[1]) )
atoms.set_calculator( calc )
e = atoms.get_potential_energy()
f = atoms.get_forces()

if mpi.world.rank == 0:
    writer = TrajectoryWriter( sys.argv[2], mode='a', atoms=atoms )
    writer.write()

    efile = open('qmenergy.txt', 'a')
    efile.write( str(e) + '\n')
    efile.close()

    ffile = open('qmforce.txt', 'a')
    ffile.write( str(f) + '\n' )
    ffile.close()
