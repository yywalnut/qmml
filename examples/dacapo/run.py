#!/usr/bin/env python
import sys, os
sys.path.append('../..')

from QMML import QMML
from amp import Amp
from amp.descriptor.gaussian import Gaussian
from amp.model.neuralnetwork import NeuralNetwork
from ase import Atoms, io
from ase.visualize import view
from ase.constraints import FixAtoms
from ase.optimize import BFGS
from ase.calculators.jacapo import Jacapo

def readimages(path):
    timages = []
    for ftraj in os.listdir(path):
        images = io.read(os.path.join(path, ftraj),
                        index=':')
        timages.extend(images)
    return timages

atoms = io.read('./Cu_1.traj')
atoms.set_pbc((1,1,1))
indices=[atom.index for atom in atoms if atom.tag==0]
atoms.set_constraint(FixAtoms(indices=indices))

selection = range(27,54)
ampcalc = Amp.load('./calc.amp')
qmcalc = Jacapo(nc='out.nc', #nc output file
                pw=340.15, #planewave cutoff
                dw=500.00, #density cutoff
                nbands=None, # number of bands
                kpts=(2,4,1), # k points
                xc='RPBE', #exchange correlation method
                ft=0.1, #Fermi temperature
                symmetry=False,
                dipole={'status':True,
                        'mixpar':0.2,
                        'initval':0.0,},
                convergence={'energy':0.00001,
                             'density':0.0001,
                             'occupation':0.001,
                             'maxsteps':None,
                             'maxtime':None},
                spinpol=False)
retrain={'energy_maxresid': 4.0e-3,
         'force_maxresid': 5.0e-2}
oldimages = readimages('/gpfs/data/ap31/coppertraj/dacapo_copper')

calc = QMML(selection=selection,
            qmcalc=qmcalc,
            mmcalc=ampcalc,
            qmpbc=(1,1,1),
            qmvac=5, #or use 'qmcell' to specify cell
            retrain = retrain,
            traintraj = oldimages)

atoms.set_calculator(calc)
dyn = BFGS(atoms, logfile='qn.log', trajectory='qn.traj' )
dyn.run(fmax=0.05)
print atoms.get_potential_energy()
